package be.mustafa.vdab.house.household;

import be.mustafa.vdab.house.household.config.CleaningServiceTestConfig;
import be.mustafa.vdab.house.household.mocks.CleaningToolMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(CleaningServiceTestConfig.class)
class CleaningServiceImplTest {

    @Autowired
    private CleaningService testCleaner;

    @Autowired
    public CleaningToolMock mocktool;

    @Test
    public void testDoJob(){
        testCleaner.clean();
        assertTrue(mocktool.isCalled());
    }



}
