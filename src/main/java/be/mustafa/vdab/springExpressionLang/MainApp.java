package be.mustafa.vdab.springExpressionLang;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

public class MainApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        Computer comp = ctx.getBean("computer", Computer.class);

        System.out.println(comp);
    }
}
