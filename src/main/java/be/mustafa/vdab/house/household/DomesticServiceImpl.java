package be.mustafa.vdab.house.household;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Locale;
import java.util.logging.Logger;

@Service("domesticService")
//@Qualifier("domestic")
public class DomesticServiceImpl implements DomesticService {
    @Autowired
    private GardeningService gardeningService;
    @Autowired
    //@Qualifier("robot")
    private CleaningService cleaningService;
    @Autowired
    private CookingService cook;
    private Logger logger;
    private MessageSource ms;
    private Locale locale;

    @Autowired
    public void setLogger(Logger logger){
        this.logger = logger;
    }
    @Autowired
    public void setMessageSource(MessageSource ms){
        this.ms = ms;
    }

    @Value("#{new java.util.Locale('ch')}")
    public void setLocale(Locale locale){
        this.locale = locale;
    }

    @Override
    @EventListener(classes = ContextStartedEvent.class)
    @Order(1)
    public void runHousehold() {
        logger.info("Running household");
        gardeningService.gardening();
        try{
            Thread.sleep(2000);
        }catch (InterruptedException ie){

        }
        cleaningService.clean();
        try{
            Thread.sleep(3000);
        }catch (InterruptedException ie){

        }
    }

    public void setGardeningService(GardeningService gardeningService){
        this.gardeningService = gardeningService;
    }

    public void setCleaningService(CleaningService cleaningService){
        this.cleaningService = cleaningService;
    }



    @PostConstruct
    public void init(){
        logger.info("Domestic service has been inistialized.");
    }

    @PreDestroy
    public void destroy(){
        logger.info("Domestic service has been destroyed.");
        logger.info(ms.getMessage("welcome", new Object[]{12}, locale));
    }



}
