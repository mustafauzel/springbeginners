package be.mustafa.vdab.house.household.mocks;

import be.mustafa.vdab.house.tools.GardeningTool;

public class GardeningToolMock implements GardeningTool {
    private boolean gardenCalled;

    @Override
    public void doGardenJob() {
        gardenCalled = true;
    }

    public boolean isCalled(){
        return gardenCalled;
    }
}
