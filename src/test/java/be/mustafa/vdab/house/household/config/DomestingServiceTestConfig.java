package be.mustafa.vdab.house.household.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = {"be.mustafa.vdab.house", "be.mustafa.vdab.aspecting"}, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*TestConfig.*"))
public class DomestingServiceTestConfig {
}
