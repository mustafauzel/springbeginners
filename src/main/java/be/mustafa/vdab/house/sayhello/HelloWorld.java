package be.mustafa.vdab.house.sayhello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HelloWorld implements Hello {
    @Value("${mustafa}")
    String name;
    @Override
    public String hello() {
        return "Hello " + name;
    }
}
