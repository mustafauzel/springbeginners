package be.mustafa.vdab.aspecting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class MusicMakerImpl implements MusicMaker {
    private static String MUSIC = "*Singing songs, tagging along, all with my big fat SCHLONG !*";

    @Autowired
    Logger logger;
    @Override
    public void makeMusic() {
        //logger.info(MUSIC);
        System.out.println(MUSIC);
    }
}
