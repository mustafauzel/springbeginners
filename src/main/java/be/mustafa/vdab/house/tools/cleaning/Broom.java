package be.mustafa.vdab.house.tools.cleaning;

import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
//@Primary
@Profile("smallHouse | bigHouse")
//@Qualifier("broomie")
@Order(2)
public class Broom implements CleaningTool {
    private Logger logger;

    @Autowired
    public void setLogger(Logger logger){
        this.logger = logger;
    }

    public Broom(Logger logger){
        logger.info("Broom CREATED!");
    }

    public void init(){
        logger.info("Initializing Broom.");
    }
    public void destroy(){
        logger.info("Destroy Broom.");
    }
    @Override
    public void doCleanJob() {
        logger.info("Sweep sweep");
    }

}
