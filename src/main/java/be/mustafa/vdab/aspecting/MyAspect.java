package be.mustafa.vdab.aspecting;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAspect {
    @Before("execution(* *.sayHello(..))")
    public void before(){
        System.out.println("BEFORE HELLO");
    }

    @After("execution(* *.sayHello(..))")
    public void after(){
        System.out.println("AFTER HELLO");
    }

    @AfterReturning("execution(* *.sayHello(..))")
    public void afterReturning(){
        System.out.println("AFTER RETURNING HELLO");
    }

    @AfterThrowing("execution(* *.sayHello(..))")
    public void afterThrowing(){
        System.out.println("AFTER THROWING HELLO");
    }
}
