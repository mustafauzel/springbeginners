package be.mustafa.vdab.house.tools.gardening;

import be.mustafa.vdab.house.tools.GardeningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
//@Primary
@Profile("!bigHouse & !smallHouse")
//@Qualifier("hoe")
@Order(1)
public class GardeningHoe implements GardeningTool {
    @Autowired
    private Logger logger;

    public GardeningHoe(Logger logger){
        logger.info("Gardening hoe CREATED");
    }
    @Override
    public void doGardenJob() {
        logger.info("Gardening like a hoe... I mean WITH a hoe.");
    }

    @Override
    public void init() {
        logger.info("Gardening hoe initialized.");
    }

    @Override
    public void destroy() {
        logger.info("Gardening hoe destroyed.");
    }
}
