package be.mustafa.vdab.house.tools.cleaning;

import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
//@Qualifier("sucky")
@Profile("bigHouse")
@Order(3)
public class VacuumCleaner implements CleaningTool {
    @Autowired
    private Logger logger;

    public VacuumCleaner(Logger logger){
        logger.info("Vacuumcleaner CREATED!");
    }

    @Override
    public void doCleanJob() {
        logger.info("\nZzzzzzoem");
    }
}
