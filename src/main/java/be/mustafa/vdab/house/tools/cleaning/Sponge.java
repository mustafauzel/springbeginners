package be.mustafa.vdab.house.tools.cleaning;

import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
//@Qualifier("bobby")
@Profile("bigHouse")
@Order(4)
public class Sponge implements CleaningTool {

    private Logger logger;

    public Sponge(Logger logger){
        logger.info("Sponge CREATED!");
    }

    @Autowired
    public void setLogger(Logger logger){
        this.logger = logger;
    }

    @Override
    public void doCleanJob() {
        logger.info("Scrub scrub with Spongy Bob!");
    }
}

