package be.mustafa.vdab.house.household;

import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.logging.Logger;

@Component
@Lazy
@Profile("bigHouse")
//@Qualifier("robot")
public class CleaningRobot implements CleaningService {
    @Autowired
    private List<CleaningTool> tools;
    @Autowired
    Logger logger;

    //@Autowired
    public void setCleaningTools(List<CleaningTool> tools){
        this.tools = tools;
    }


    @Override
    public void clean() {
        logger.info("Cleaning the house...");
        tools.forEach(CleaningTool::doCleanJob);
        logger.info("Shutting down, battery 98 percent.");
    }

    @PostConstruct
    public void init(){
        logger.info("Cleaning robot has been initialized.");
    }

    @PreDestroy
    public void destroy(){
        logger.info("Cleaning robot has been destroyed.");
    }
}
