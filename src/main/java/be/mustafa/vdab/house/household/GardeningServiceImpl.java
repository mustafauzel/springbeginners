package be.mustafa.vdab.house.household;

import be.mustafa.vdab.events.LunchEvent;
import be.mustafa.vdab.house.tools.GardeningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Service("gardserv")
public class GardeningServiceImpl implements GardeningService {
    @Autowired
    private Logger logger;
    private GardeningTool tool;

    @Autowired
    public GardeningServiceImpl(){

    }

    @Override
    public void gardening() {
        tool.doGardenJob();
    }

    @Autowired
    public void setGardeningTool(GardeningTool tool){
        this.tool = tool;
    }

    @PostConstruct
    public void init(){
        logger.info("Gardening service has been initialized.");
    }

    @PreDestroy
    public void destroy(){
        logger.info("Gardening service has been destroyed.");
    }

    @EventListener(classes = LunchEvent.class)
    @Order(2)
    public void onLunchEvent(){
        logger.info("Lunch time for the gardening crew.");
    }
}
