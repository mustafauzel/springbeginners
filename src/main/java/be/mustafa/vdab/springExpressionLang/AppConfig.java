package be.mustafa.vdab.springExpressionLang;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.ConfigurablePropertyResolver;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class AppConfig {

}
