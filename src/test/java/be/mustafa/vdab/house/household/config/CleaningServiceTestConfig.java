package be.mustafa.vdab.house.household.config;

import be.mustafa.vdab.house.household.CleaningService;
import be.mustafa.vdab.house.household.CleaningServiceImpl;
import be.mustafa.vdab.house.household.mocks.CleaningToolMock;
import be.mustafa.vdab.house.logging.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import java.util.logging.Logger;

@Configuration
@PropertySource("classpath:application.properties")
@Import(LoggerFactory.class)
public class CleaningServiceTestConfig {
    @Bean
    public CleaningToolMock mock(){
        return new CleaningToolMock();
    }

    @Bean
    public CleaningService testCleaner(){
        return new CleaningServiceImpl();
    }

}
