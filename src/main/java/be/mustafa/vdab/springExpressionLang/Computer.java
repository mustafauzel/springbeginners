package be.mustafa.vdab.springExpressionLang;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;

@Component
public class Computer {
    //"#{T(java.time.LocalDate).now()}"
    private LocalDate purchaseDate;
    private URL vendorUrl;
    private double price;
    private int memory;
    private String cpu;
    private int processors;
    private String ownerName;
    private String os;
    private List<String> features;

    public LocalDate getpurchaseDate(){
        return this.purchaseDate;
    }
    @Value("#{T(java.time.LocalDate).of(2012,09,29)}")
    public void setpurchaseDate(LocalDate purchaseDate){
        this.purchaseDate = purchaseDate;
    }
    public URL getVendorUrl(){
        return this.vendorUrl;
    }
    @Value("#{new java.net.URL('http://www.google.com')}")
    public void setVendorUrl(URL vendorUrl){
        this.vendorUrl = vendorUrl;
    }
    public double getPrice(){
        return this.price;
    }
    @Value("${price}")
    public void setPrice(double price){
        this.price = price;
    }
    public int getMemory(){
        return this.memory;
    }
    @Value("#{T(Runtime).getRuntime().maxMemory()}")
    public void setMemory(int memory){
        this.memory = memory;
    }
    public String getCpu(){
        return this.cpu;
    }
    @Value("#{T(System).getenv('PROCESSOR_IDENTIFIER')}")
    public void setCpu(String cpu){
        this.cpu = cpu;
    }
    public int getProcessors(){
        return this.processors;
    }
    @Value("#{T(Runtime).getRuntime().availableProcessors()}")
    public void setProcessors(int processors){
        this.processors = processors;
    }
    public String getOwnerName(){
        return this.ownerName;
    }
    @Value("#{systemProperties['user.name']}")
    public void setOwnerName(String ownerName){
        this.ownerName = ownerName;
    }
    public String getOs(){
        return this.os;
    }
    @Value("#{systemProperties['os.name']}")
    public void setOs(String os){
        this.os = os;
    }
    public List<String> getFeatures(){
        return this.features;
    }
    @Value("#{{'Camera', 'USB 3.0', 'HD screen'}}")
    public void setFeatures(List<String> features){
        this.features = features;
    }

    @Override
    public String toString() {
        return "Computer of " + ownerName +
                " with purchase date of " + purchaseDate +
                " and vendor url: " + vendorUrl +
                "\nPrice: € " + price +
                "\nMemory: " + memory +
                "\nCpu: " + cpu +
                "\nProcessors: " + processors +
                "\nOwnerName: " + ownerName +
                "\nOs: " + os +
                "\nFeatures: " + features
                ;
    }
}
