package be.mustafa.vdab.aspecting;

public interface MyInterface {
    String sayHello(String name);
    String sayGoodbye(String name);
}
