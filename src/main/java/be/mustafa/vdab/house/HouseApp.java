package be.mustafa.vdab.house;

import be.mustafa.vdab.AppConfig;
import be.mustafa.vdab.aspecting.MusicMaker;
import be.mustafa.vdab.aspecting.MusicMakerImpl;
import be.mustafa.vdab.aspecting.MyInterface;
import be.mustafa.vdab.house.household.DomesticService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import be.mustafa.vdab.aspecting.MusicMaker;

public class HouseApp {
    public static final String NAME = "Mustafa";
    public static void main(String[] args) {
        //ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("smallHouse");
        ctx.register(AppConfig.class);
        ctx.refresh();

        /*MyInterface bean = ctx.getBean(MyInterface.class);
        System.out.println(bean.sayHello(NAME));
        System.out.println(bean.sayGoodbye(NAME));*/

        //Locale locale = Locale.getDefault();
        /*Locale lokaal = new Locale("en");
        String message = ctx.getMessage("welcome", new Object[]{12}, lokaal);
        System.out.println(message);*/

        //ctx.start();

        DomesticService service = ctx.getBean("domesticService", DomesticService.class);
        service.runHousehold();
        ((MusicMaker) service).makeMusic();


        //ctx.publishEvent(new LunchEvent());

        //Hello s = ctx.getBean(Hello.class);
        //System.out.println(s.hello());







        ctx.close();
    }
}
