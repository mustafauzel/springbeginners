package be.mustafa.vdab.house.household.config;

import be.mustafa.vdab.house.household.GardeningService;
import be.mustafa.vdab.house.household.GardeningServiceImpl;
import be.mustafa.vdab.house.household.mocks.GardeningToolMock;
import be.mustafa.vdab.house.logging.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@Import(LoggerFactory.class)
public class GardenServiceTestConfig {
    @Bean
    public GardeningToolMock gardenmock(){
        return new GardeningToolMock();
    }

    @Bean
    public GardeningService testGardener(){
        return new GardeningServiceImpl();
    }
}
