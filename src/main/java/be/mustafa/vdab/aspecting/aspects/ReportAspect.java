package be.mustafa.vdab.aspecting.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ReportAspect {
    @Before("execution(* be.mustafa.vdab.house.household.DomesticServiceImpl.runHousehold())")
    public void beforeHousehold(){
        System.out.println("BEFORE HOUSEHOLD");
    }

    @After("execution(* be.mustafa.vdab.house.household.DomesticServiceImpl.runHousehold())")
    public void afterHousehold(){
        System.out.println("AFTER HOUSEHOLD");
    }

    @AfterThrowing("execution(* be.mustafa.vdab.house.household.DomesticServiceImpl.runHousehold())")
    public void afterHouseholdThrowing(){
        System.out.println("AFTER EXCEPTION HOUSEHOLD");
    }


}
