package be.mustafa.vdab.aspecting.aspects;

import be.mustafa.vdab.aspecting.MusicMaker;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MusicAspect {
    @DeclareParents(value = "be.mustafa.vdab.house.household.DomesticServiceImpl", defaultImpl = be.mustafa.vdab.aspecting.MusicMakerImpl.class)
    public static MusicMaker music;

    //Het injecteert een MusicMakerImpl in DomesticServiceImpl voor alle PUBLIC en PROTECTED methoden!

}
