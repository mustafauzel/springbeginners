package be.mustafa.vdab;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
@EnableAspectJAutoProxy
//@Import(AppConfigB.class) NOG NIET AANGEMAAKT
public class AppConfig {

    @Bean
    public MessageSource messageSource(){
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("housekeeping");
        return messageSource;
    }
    /*@Bean
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
    public CleaningTool duster(){
        return new DisposableDuster();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Primary
    public CleaningTool broom(){
        return new Broom();
    }

    @Bean
    @Lazy
    public CleaningTool vacuum(){
        return new VacuumCleaner();
    }

    @Bean
    public CleaningTool sponge(){
        return new Sponge();
    }

    @Bean
    public GardeningTool lawnmower(){
        return new LawnMower();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Primary
    public GardeningTool hoe(){
        return new GardeningHoe();
    }

    @Bean
    public GardeningService lars(GardeningTool tool){
        GardeningServiceImpl service = new GardeningServiceImpl();
        service.setGardeningTool(tool);
        return service;
    }

    @Bean
    @Primary
    public GardeningService karen(GardeningTool tool){
        GardeningServiceImpl service = new GardeningServiceImpl();
        service.setGardeningTool(tool);
        return service;
    }

    @Bean
    @Primary
    public CleaningService jill(CleaningTool tool){
        CleaningServiceImpl service = new CleaningServiceImpl();
        service.setTool(tool);
        return service;
    }

    @Bean(name = {"pommeke","pommeline","pomtje"})
    @Lazy
    public CleaningService pommeline(){
        CleaningServiceImpl service = new CleaningServiceImpl();
        service.setTool(vacuum());
        return service;
    }

    @Bean
    public CleaningService geoffrey(){
        CleaningServiceImpl service = new CleaningServiceImpl();
        service.setTool(sponge());
        return service;
    }

    @Bean
    public CleaningService bagera(){
        CleaningServiceImpl service = new CleaningServiceImpl();
        service.setTool(duster());
        return service;
    }

    @Bean
    public CleaningService bagheera(){
        CleaningServiceImpl service = new CleaningServiceImpl();
        service.setTool(duster());
        return service;
    }

    @Bean
    public DomesticService domesticService(CleaningService cleaning, GardeningService gardening){
        DomesticServiceImpl service1 = new DomesticServiceImpl();
        service1.setCleaningService(cleaning);
        service1.setGardeningService(gardening);
        return service1;
    }*/
}
