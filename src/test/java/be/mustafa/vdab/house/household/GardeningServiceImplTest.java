package be.mustafa.vdab.house.household;

import be.mustafa.vdab.house.household.config.GardenServiceTestConfig;
import be.mustafa.vdab.house.household.mocks.GardeningToolMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(GardenServiceTestConfig.class)
class GardeningServiceImplTest {
    @Autowired
    private GardeningService service;

    @Autowired
    private GardeningToolMock gardentool;

    @Test
    public void testGarden(){
        service.gardening();
        assertTrue(gardentool.isCalled());
    }

}
