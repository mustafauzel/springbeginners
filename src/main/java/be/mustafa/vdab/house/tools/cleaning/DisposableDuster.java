package be.mustafa.vdab.house.tools.cleaning;

import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
@Profile("bigHouse")
//@Qualifier("dusty")
@Order(1)
public class DisposableDuster implements CleaningTool {
    private boolean used = false;
    @Autowired
    private Logger logger;

    public DisposableDuster(Logger logger){
        logger.info("Disposable duster CREATED!");
    }

    @Override
    public void init() {
        logger.info("Initializing Disposable duster.");

    }

    @Override
    public void destroy() {
        logger.info("Destroying Disposable duster.");
    }

    @Override
    public void doCleanJob(){
        if(used){
            logger.info("\nTake a new one. I'm dirty.");

        }else{
            logger.info("\nCleaning with disposable duster...");
            used = true;
        }
    }
}
