package be.mustafa.vdab.house.tools;

public interface GardeningTool {
    void doGardenJob();
    default void init(){

    }
    default void destroy(){

    }
}
