package be.mustafa.vdab.aspecting;

public interface MusicMaker {
    void makeMusic();
}
