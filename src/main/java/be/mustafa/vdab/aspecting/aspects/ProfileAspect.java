package be.mustafa.vdab.aspecting.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

@Component
@Aspect
public class ProfileAspect {
    @Around("execution(* be.mustafa.vdab.house.household.DomesticServiceImpl.runHousehold())")
    public Object around(ProceedingJoinPoint pjp) throws Throwable{
        LocalTime startTime = LocalTime.now();
        Object returntype = pjp.proceed();
        LocalTime endTime = LocalTime.now();
        System.out.println(ChronoUnit.MILLIS.between(startTime, endTime));
        return returntype;
    }
}
