package be.mustafa.vdab.house.tools;

public interface CleaningTool {
    void doCleanJob();
    default void init(){

    }
    default void destroy(){

    }
}
