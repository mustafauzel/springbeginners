package be.mustafa.vdab.house.household.smallhouse;

import be.mustafa.vdab.house.household.CleaningService;
import be.mustafa.vdab.house.household.CleaningServiceImpl;
import be.mustafa.vdab.house.household.GardeningService;
import be.mustafa.vdab.house.household.GardeningServiceImpl;
import be.mustafa.vdab.house.household.config.DomestingServiceTestConfig;
import be.mustafa.vdab.house.household.mocks.CleaningToolMock;
import be.mustafa.vdab.house.logging.LoggerFactory;
import be.mustafa.vdab.house.tools.CleaningTool;
import be.mustafa.vdab.house.tools.GardeningTool;
import be.mustafa.vdab.house.tools.cleaning.Broom;
import be.mustafa.vdab.house.tools.gardening.LawnMower;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;


@SpringJUnitConfig(DomestingServiceTestConfig.class)
@Import(LoggerFactory.class)
@ActiveProfiles("smallHouse")
public class DomesticServiceImplTest {
    @Autowired
    public CleaningService cleaningService;
    @Autowired
    public CleaningTool cleaningTool;
    @Autowired
    public GardeningService gardeningService;
    @Autowired
    public GardeningTool gardeningTool;

    @Test
    public void testHousehold(){
        cleaningService.clean();
        gardeningService.gardening();
        assertTrue(cleaningTool.getClass() == Broom.class);
        assertTrue(gardeningTool.getClass() == LawnMower.class);
    }
}
