package be.mustafa.vdab.aspecting;

import org.springframework.stereotype.Component;

@Component
public class MyBean implements MyInterface {
    @Override
    public String sayHello(String name) {
        return String.format("Hello %s", name);
    }

    @Override
    public String sayGoodbye(String name) {
        return String.format("Goodbye %s", name);
    }
}
