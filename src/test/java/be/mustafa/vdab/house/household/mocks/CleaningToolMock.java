package be.mustafa.vdab.house.household.mocks;

import be.mustafa.vdab.house.tools.CleaningTool;

public class CleaningToolMock implements CleaningTool {
    private boolean cleanCalled;

    @Override
    public void doCleanJob(){
        cleanCalled = true;
    }

    public boolean isCalled(){
        return cleanCalled;
    }
}
