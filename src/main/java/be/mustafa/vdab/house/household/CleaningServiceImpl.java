package be.mustafa.vdab.house.household;

import be.mustafa.vdab.events.LunchEvent;
import be.mustafa.vdab.house.tools.CleaningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Service
//@Primary
@Profile("smallHouse")
//@Qualifier("cleaningservice")
public class CleaningServiceImpl implements CleaningService {
    private CleaningTool tool;
    private float rate;
    @Autowired
    private Logger logger;


    public CleaningServiceImpl(){
        
    }


    @Value("${rate}")
    public void setRate(float rate){
        this.rate = rate;
    }

    @Autowired
    public void setCleaningTool(CleaningTool tool){
        this.tool = tool;
    }

    @Override
    public void clean() {
        tool.doCleanJob();
    }

    @PostConstruct
    public void init(){
        logger.info("Cleaning service has been initialized. Rate = " + rate);
    }

    @PreDestroy
    public void destroy(){
        logger.info("Cleaning service has been destroyed.");
    }

    @EventListener(classes = LunchEvent.class)
    @Order(1)
    public void onLunchEvent(){
        logger.info("Lunch time for crew.");
    }
}
