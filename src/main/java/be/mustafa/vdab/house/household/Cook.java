package be.mustafa.vdab.house.household;

import be.mustafa.vdab.events.LunchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Profile("smallHouse | bigHouse")
public class Cook implements CookingService {
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private Logger logger;

    @Override
    public void makeLunch(){
        startCooking();
        try{
            Thread.sleep(1000);
        } catch (InterruptedException ie){

        }
        publisher.publishEvent(new LunchEvent());
    }

    @EventListener(classes = ContextStartedEvent.class)
    @Order(2)
    public void startCooking(){
        //logger.info("START COOKING");
        System.out.println("START COOKING");
        makeLunch();
    }
}
