package be.mustafa.vdab.house.tools.gardening;

import be.mustafa.vdab.house.tools.GardeningTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Profile("smallHouse")
//@Qualifier("mo")
@Order(2)
public class LawnMower implements GardeningTool {
    @Autowired
    private Logger logger;

    public LawnMower(Logger logger){
        logger.info("LawnMower CREATED");
    }

    @Override
    public void doGardenJob() {
        logger.info("Mowing the lawn, mowing the lawn...");
    }
}
